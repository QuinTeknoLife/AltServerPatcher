﻿# AltServerPatcher

A patcher for the [AltServer application](https://altstore.io) to allow installation of a third-party IPA file.