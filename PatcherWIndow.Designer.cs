﻿namespace AltServerPatcher
{
    partial class PatcherWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PatcherWindow));
            this.UtilityLabel = new System.Windows.Forms.Label();
            this.IPASelector = new System.Windows.Forms.ComboBox();
            this.PatchButton = new System.Windows.Forms.Button();
            this.IPAURLLabel = new System.Windows.Forms.Label();
            this.IPAURLBox = new System.Windows.Forms.TextBox();
            this.RestoreOriginalButton = new System.Windows.Forms.LinkLabel();
            this.WhoTFMadeThis = new System.Windows.Forms.Label();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.StatusProgress = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // UtilityLabel
            // 
            this.UtilityLabel.AutoSize = true;
            this.UtilityLabel.Location = new System.Drawing.Point(12, 16);
            this.UtilityLabel.Name = "UtilityLabel";
            this.UtilityLabel.Size = new System.Drawing.Size(77, 13);
            this.UtilityLabel.TabIndex = 0;
            this.UtilityLabel.Text = "Utility to Install:";
            // 
            // IPASelector
            // 
            this.IPASelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.IPASelector.FormattingEnabled = true;
            this.IPASelector.Items.AddRange(new object[] {
            "AltStore (default)",
            "unc0ver",
            "Chimera",
            "Electra",
            "doubleh3lix",
            "doubleh3lix+sockport2",
            "h3lix",
            "Phoenix",
            "etasonJB",
            "HomeDepot-9.1-9.3.4",
            "HomeDepot-8.4.1",
            "yalu102",
            "yalu+mach_portal",
            "yaluX+extra_recipe",
            "ChimeraTV",
            "ElectraTV",
            "backr00m",
            "Custom IPA Link"});
            this.IPASelector.Location = new System.Drawing.Point(95, 12);
            this.IPASelector.Name = "IPASelector";
            this.IPASelector.Size = new System.Drawing.Size(261, 21);
            this.IPASelector.TabIndex = 1;
            this.IPASelector.SelectedIndexChanged += new System.EventHandler(this.IPASelector_SelectedIndexChanged);
            // 
            // PatchButton
            // 
            this.PatchButton.Location = new System.Drawing.Point(362, 11);
            this.PatchButton.Name = "PatchButton";
            this.PatchButton.Size = new System.Drawing.Size(75, 23);
            this.PatchButton.TabIndex = 2;
            this.PatchButton.Text = "Patch!";
            this.PatchButton.UseVisualStyleBackColor = true;
            this.PatchButton.Click += new System.EventHandler(this.PatchButton_Click);
            // 
            // IPAURLLabel
            // 
            this.IPAURLLabel.AutoSize = true;
            this.IPAURLLabel.Location = new System.Drawing.Point(12, 42);
            this.IPAURLLabel.Name = "IPAURLLabel";
            this.IPAURLLabel.Size = new System.Drawing.Size(95, 13);
            this.IPAURLLabel.TabIndex = 3;
            this.IPAURLLabel.Text = "IPA URL (max 55):";
            // 
            // IPAURLBox
            // 
            this.IPAURLBox.Location = new System.Drawing.Point(113, 39);
            this.IPAURLBox.MaxLength = 55;
            this.IPAURLBox.Name = "IPAURLBox";
            this.IPAURLBox.ReadOnly = true;
            this.IPAURLBox.Size = new System.Drawing.Size(324, 20);
            this.IPAURLBox.TabIndex = 4;
            // 
            // RestoreOriginalButton
            // 
            this.RestoreOriginalButton.AutoSize = true;
            this.RestoreOriginalButton.Location = new System.Drawing.Point(12, 96);
            this.RestoreOriginalButton.Name = "RestoreOriginalButton";
            this.RestoreOriginalButton.Size = new System.Drawing.Size(82, 13);
            this.RestoreOriginalButton.TabIndex = 5;
            this.RestoreOriginalButton.TabStop = true;
            this.RestoreOriginalButton.Text = "Restore Original";
            this.RestoreOriginalButton.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RestoreOriginalButton_LinkClicked);
            // 
            // WhoTFMadeThis
            // 
            this.WhoTFMadeThis.AutoSize = true;
            this.WhoTFMadeThis.Location = new System.Drawing.Point(249, 96);
            this.WhoTFMadeThis.Name = "WhoTFMadeThis";
            this.WhoTFMadeThis.Size = new System.Drawing.Size(188, 13);
            this.WhoTFMadeThis.TabIndex = 6;
            this.WhoTFMadeThis.Text = "AltServer Patcher by InvoxiPlayGames";
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.BackColor = System.Drawing.Color.Transparent;
            this.StatusLabel.Location = new System.Drawing.Point(12, 65);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(41, 13);
            this.StatusLabel.TabIndex = 7;
            this.StatusLabel.Text = "Ready!";
            // 
            // StatusProgress
            // 
            this.StatusProgress.Location = new System.Drawing.Point(15, 81);
            this.StatusProgress.MarqueeAnimationSpeed = 10;
            this.StatusProgress.Name = "StatusProgress";
            this.StatusProgress.Size = new System.Drawing.Size(422, 10);
            this.StatusProgress.TabIndex = 8;
            // 
            // PatcherWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 118);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.StatusProgress);
            this.Controls.Add(this.WhoTFMadeThis);
            this.Controls.Add(this.RestoreOriginalButton);
            this.Controls.Add(this.IPAURLBox);
            this.Controls.Add(this.IPAURLLabel);
            this.Controls.Add(this.PatchButton);
            this.Controls.Add(this.IPASelector);
            this.Controls.Add(this.UtilityLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatcherWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AltServer Patcher";
            this.Load += new System.EventHandler(this.PatcherWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UtilityLabel;
        private System.Windows.Forms.ComboBox IPASelector;
        private System.Windows.Forms.Button PatchButton;
        private System.Windows.Forms.Label IPAURLLabel;
        private System.Windows.Forms.TextBox IPAURLBox;
        private System.Windows.Forms.LinkLabel RestoreOriginalButton;
        private System.Windows.Forms.Label WhoTFMadeThis;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.ProgressBar StatusProgress;
    }
}

